#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 10:59:51 2022

@author: Sophie Turner
"""


# used to perform the x-ray scan for SPEC
# =============================================================================
# GENERATE X-RAY PTS --- X,Y,Z Direction
# =============================================================================

import os
from tkinter import *
from tkinter.filedialog import asksaveasfilename
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import argparse
import readline
import sys

count=0

def OuputForSpec(file, X, Y, Z, ome_pos, ome_corr, theta, Time): #sets format for array, sets up what you wanna record 
    """
    This fuction takes the inputs and writes them as column vectors into the specified file.
        inputs: file = file name
               X = X motor array
               Y = Y motor array
               Z = Z motor array 
               ome_pos = omega_position
               ome_corr = omega correction
               theta = theta constant 
               Time = Dwell time 
    """
    global count
    
    n_points = X.shape[0]
    
    if np.isscalar(Time):
        Time = np.ones(n_points) * Time
        
    if np.isscalar(ome_pos):
        ome_pos = np.ones(n_points) * ome_pos
        
    if np.isscalar(ome_corr):
        ome_corr = np.ones(n_points) * ome_corr
        
    if np.isscalar(theta):
        theta = np.ones(n_points) * theta
    
                
    # File Writing 
    with open(file, 'a+') as file1:
        for ii in range(n_points):
            count+=1 # global count to keep track of a continuous iteration of data point numbers
            file1.write( "%s %f %f %f %f %f %f %f \n" % ((count), X[ii], Y[ii], Z[ii], ome_pos[ii], ome_corr[ii], theta[ii], Time[ii]))
    
    
def dir_path(path):
    """ Turns the output_path argparse input into a directory path """
    if os.path.isdir(path):
        return path
    else:
        return False 
    
# Save as Window 
def save_file():
    """ Saving the OuputForSpec data into a file for the Nominal and omega correction position of the plate """
    ff=asksaveasfilename(initialfile = "Untitled.txt", defaultextension='.txt', 
                    filetypes=[("All Files", "*.*"), ("Text Documents", ".txt")], 
                    initialdir=args.output_path)
    if (type(args.ome_corr)==int):
        OuputForSpec(ff, X, Y, Z, args.ome, args.ome_corr, args.theta, args.Time)
    else:
        for i in args.ome_corr: # appening the list of omega corrections to the file
            OuputForSpec(ff, X, Y, Z, args.ome, i, args.theta, args.Time)


# Creating a save as gui
def nom_gui(): 
    """ Creating a GUI window for saving data of all positions of plate """
    win = Tk() #create tinker window for front plate
    win.geometry("250x200") #set the geometry of the tkinter window

    win.title("Plate Positions")
    btn = Button(win, text ="Save", command=save_file)
    btn.pack(pady=50)  
     
    win.mainloop()
    
# =============================================================================
# SETUP -- USER INPUT
# =============================================================================

print("Please Input the Desired Parameters. Enter -h for help")

parser = argparse.ArgumentParser(description=('===== X-RAY Scan Parameters \n *Note:In Chess reference frame ====='))
parser.add_argument('--order',
                    choices=('xyz', 'yzx', 'zxy', 'xzy', 'zyx', 'yxz'),
                    help='Scan axis order (name of fastest scanned dimension first, slowest dimension last).',
                    default='xyz')
for dim in ('x', 'y', 'z'):
    parser.add_argument(f'--{dim}',
                        type=float,
                        help=f'Nominal center of the {dim} range',
                        required=True)
    parser.add_argument(f'--n{dim}',
                        type=int,
                        help=f'Number of intervals in the {dim} direction',
                        required=True )
    parser.add_argument(f'--Rel{dim}',
                        type=float, 
                        help=f'Length of the {dim} range OR min and max values of the {dim} range relative to the nominal center', 
                        nargs='+', 
                        required=True)
parser.add_argument('--ome', type=float, metavar='Omega', help='Omega position where the sample is perpendicular to the beam in the nominal position', required = True)
parser.add_argument('--ome_corr', type=float, metavar='Omega Correction', nargs='+', help='Correction angle to be applied to the omega position to get a particular data point perpendicular to the beam', 
                    default=0)
parser.add_argument('--theta', type=float, help='Theta value', required=True)
parser.add_argument('--Time', type=float, help='Dwell time')
parser.add_argument('--output_path', type=dir_path, help='Directory path to save the file to', default=False)

args = parser.parse_args()

# =============================================================================
# GENERATE X-RAY PTS
# =============================================================================

# Construct arrays of unique motor values in each dimension
unique_motor_vals = {}
for dim in ('x', 'y', 'z'):
    center = getattr(args, f'{dim}')
    relative_range = getattr(args, f'Rel{dim}')
    n_points = getattr(args, f'n{dim}') + 1

    if len(relative_range) > 2:
        sys.exit(f"Rel{dim} only takes two numbers")
    elif len(relative_range) == 2:
        unique_motor_vals[dim] = np.linspace(*relative_range, n_points) + center
    elif len(relative_range) == 1:
        unique_motor_vals[dim] = np.linspace(-relative_range[0], relative_range[0], n_points) + center
        

# order from left to right is fastest to slowest (xyz)
# np.meshgrid(slowest, medium, fastest, indexting='ij)
fast, medium, slow = [unique_motor_vals[dim] for dim in args.order]
smf = np.meshgrid(slow, medium, fast, indexing='ij')
ordered_motor_vals = {}
for i,dim in enumerate(args.order):
    ordered_motor_vals[dim] = smf[2-i].flatten()

X = ordered_motor_vals['x']
Y = ordered_motor_vals['y']
Z = ordered_motor_vals['z']

n_points = ordered_motor_vals['x'].shape[0]
print(f'Number of points in the scan: {n_points}')
scan_time = args.Time * n_points / 3600
print(f'Time to scan {n_points} points: {scan_time} seconds')

# =============================================================================
# PLOT X-RAY POINTS
# =============================================================================
# gives a graph

fig, axs = plt.subplots(1, 3)
axes = (('x', 'y'), ('y', 'z'), ('x', 'z'))
for i, ax in enumerate(axes):
    axs[i].plot(ordered_motor_vals[ax[0]], ordered_motor_vals[ax[1]], '.')
    axs[i].set_xlabel(ax[0].upper())
    axs[i].set_ylabel(ax[1].upper())

fig.suptitle('Points to Scan')
fig.tight_layout()


plt.subplots_adjust(bottom=.25) # making room for buttons 

# Create buttons and their axes
save_b = Button(plt.axes([.75, .05, .2, .075]), 'Save Plate')

def save_file_mpl(event):
    root = Tk()
    root.withdraw()
    save_file()
    root.destroy()
    
cid_save_front = save_b.on_clicked(lambda event: save_file_mpl(event))

plt.show(block=True)

save_b.disconnect(cid_save_front)


                    
       




