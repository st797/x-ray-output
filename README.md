# X-ray Array Generator 

## Description
This script is intended for users scanning samples at CHESS with a x-ray. A user-friendly command line interface takes the x-ray parameters and generates an output file. The file is formatted to be understood by SPEC (the beamline control software). The x-ray is fixed so motors are used to translate and rotate the sample for mapping. Every sample is unique and requires its own array. This Python script is used to generate motor arrays to capture how the user's sample is sitting in space in an efficient way.

## Visuals
Save As GUI window with a plot of points to be scanned by the x-ray. The **Save Plate** button will save a file containing all omega corrections. 
![Plot of points to be scanned and save as GUI](x_plot.jpg)


## Installation
Clone X-ray Output Repository. <br />
Create a conda environment from the environment.yml file. <br />
Activate the conda environment. <br />
Run script. 

## Usage
The output of the script is a text file of the motor arrays. Users can save the file containing multiple omega correction values. This file is formatted to be used in SPEC as motor position instructions. 

To access the help menu for the required inputs for this script, the command line input is <br />
**python XrayOutput.py -h**

Command Line Input: <br />
**python XrayOutput.py --x 1 --y 50 --z 100 --nx 2 --nz 0 --Relx 1 5 --Rely 5 10 --Relz 1 --ome 0.7321 --theta 3.24 --Time 150 --ome_corr 45 0 30 60**

	 	Example of X-ray file
1 2.000000 55.000000 99.000000 0.732100 45.000000 3.240000 150.000000 <br />
2 4.000000 55.000000 99.000000 0.732100 45.000000 3.240000 150.000000 <br />
3 6.000000 55.000000 99.000000 0.732100 45.000000 3.240000 150.000000 <br />
4 2.000000 57.500000 99.000000 0.732100 45.000000 3.240000 150.000000 <br />
5 4.000000 57.500000 99.000000 0.732100 45.000000 3.240000 150.000000 <br />
6 6.000000 57.500000 99.000000 0.732100 45.000000 3.240000 150.000000 <br />
7 2.000000 60.000000 99.000000 0.732100 45.000000 3.240000 150.000000 <br />
8 4.000000 60.000000 99.000000 0.732100 45.000000 3.240000 150.000000 <br />
9 6.000000 60.000000 99.000000 0.732100 45.000000 3.240000 150.000000 <br />
10 2.000000 55.000000 99.000000 0.732100 0.000000 3.240000 150.000000 <br />
11 4.000000 55.000000 99.000000 0.732100 0.000000 3.240000 150.000000 <br />
12 6.000000 55.000000 99.000000 0.732100 0.000000 3.240000 150.000000 <br />
13 2.000000 57.500000 99.000000 0.732100 0.000000 3.240000 150.000000 <br />
14 4.000000 57.500000 99.000000 0.732100 0.000000 3.240000 150.000000 <br />
15 6.000000 57.500000 99.000000 0.732100 0.000000 3.240000 150.000000 <br />
16 2.000000 60.000000 99.000000 0.732100 0.000000 3.240000 150.000000 <br />
17 4.000000 60.000000 99.000000 0.732100 0.000000 3.240000 150.000000 <br />
18 6.000000 60.000000 99.000000 0.732100 0.000000 3.240000 150.000000 <br />
19 2.000000 55.000000 99.000000 0.732100 30.000000 3.240000 150.000000 <br />
20 4.000000 55.000000 99.000000 0.732100 30.000000 3.240000 150.000000 <br />
21 6.000000 55.000000 99.000000 0.732100 30.000000 3.240000 150.000000 <br />
22 2.000000 57.500000 99.000000 0.732100 30.000000 3.240000 150.000000 <br />
23 4.000000 57.500000 99.000000 0.732100 30.000000 3.240000 150.000000 <br />
24 6.000000 57.500000 99.000000 0.732100 30.000000 3.240000 150.000000 <br />
25 2.000000 60.000000 99.000000 0.732100 30.000000 3.240000 150.000000 <br />
26 4.000000 60.000000 99.000000 0.732100 30.000000 3.240000 150.000000 <br />
27 6.000000 60.000000 99.000000 0.732100 30.000000 3.240000 150.000000 <br />
28 2.000000 55.000000 99.000000 0.732100 60.000000 3.240000 150.000000 <br />
29 4.000000 55.000000 99.000000 0.732100 60.000000 3.240000 150.000000 <br />
30 6.000000 55.000000 99.000000 0.732100 60.000000 3.240000 150.000000 <br />
31 2.000000 57.500000 99.000000 0.732100 60.000000 3.240000 150.000000 <br />
32 4.000000 57.500000 99.000000 0.732100 60.000000 3.240000 150.000000 <br />
33 6.000000 57.500000 99.000000 0.732100 60.000000 3.240000 150.000000 <br />
34 2.000000 60.000000 99.000000 0.732100 60.000000 3.240000 150.000000 <br />
35 4.000000 60.000000 99.000000 0.732100 60.000000 3.240000 150.000000 <br />
36 6.000000 60.000000 99.000000 0.732100 60.000000 3.240000 150.000000 <br />

Col 0 = Data point number <br />
Col 1 = X-Position <br />
Col 2 = Y-Position <br />
Col 3 = Z-Position <br />
Col 4 = Omega (nominal point is perpendicular to x-ray) <br />
Col 5 = Omega Correction (correction angle to be applied to the omega position to get a particular data point perpendicular to the beam) <br />
Col 6 = Theta 
Col 7 = Dwell time (s) <br />


## Support
For help with this script reach out to Peter Ko, Kelly Nygren, Keara Soloway, or Rolf Verberg.

## Authors and acknowledgment
Author: Sophie Turner <br />
Thank you to my mentors Keara Soloway, Peter Ko, Rolf Verberg, and Kelly Nygren for assisting me through this project.

## Project status
This project is currently only for rectangular and linear grids. Further development can allow for irregular geometries and/or irregular intervals.
